<?php
declare(strict_types=1);

namespace PJ\Utils;

/**
 * Generuje ścieżkę bezwzględną do pliku lub katalogu.
 *
 * @param string ...$directory Nazwy kolejnych folderów.
 */
function absolutePath(string ...$directory): string
{
	$baseDir = str_replace(['/', '\\'], DIRECTORY_SEPARATOR, $_SERVER['DOCUMENT_ROOT'] ? dirname($_SERVER['DOCUMENT_ROOT']) : getcwd());
	return $baseDir.DIRECTORY_SEPARATOR.implode(DIRECTORY_SEPARATOR, $directory);
}