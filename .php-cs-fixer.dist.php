<?php
declare(strict_types=1);

use PhpCsFixer\Finder;
use PJ\PhpCsFixer\Config;

$finder = Finder::create()
	->in(__DIR__)
	->ignoreUnreadableDirs(true)
	->ignoreVCSIgnored(true);

return Config::get()
	->setFinder($finder);